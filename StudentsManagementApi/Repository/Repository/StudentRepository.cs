﻿namespace Repository
{
    using System.Data;
    using System.Data.SqlClient;
    using Dapper;
    using Model.DTOS.Student;
    using Model.Entities.Student;

    public class StudentRepository : IStudentRepository
    {
        private ConfigurationDB configurationDB;
        private SqlConnection dbConexion;

        public StudentRepository()
        {
            this.configurationDB = new ConfigurationDB();
            this.dbConexion = new SqlConnection(this.configurationDB.ConnectionString());
        }

        public StudentEntity AddSingleStudent(StudentEntity studentEntityCreated)
        {
            string procedure = "Add_New_Single_Student";
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@Name", studentEntityCreated.Name);
            parameters.Add("@LastName", studentEntityCreated.LastName);
            parameters.Add("@CountryId", studentEntityCreated.CountryId);
            parameters.Add("@CourseId", null);
            parameters.Add("@CourseAssigned", false);
            parameters.Add("@Email", studentEntityCreated.Email);
            parameters.Add("@BirthDay", studentEntityCreated.BirthDay);
            parameters.Add("@ImageUrl", studentEntityCreated.ImageUrl);
            StudentEntity userNewStudent = this.dbConexion.QuerySingle<StudentEntity>(procedure, parameters, commandType: CommandType.StoredProcedure);
            return userNewStudent;
        }

        public List<StudentDTO> FindAllStudents()
        {
            string procedure = "Select_All_Students";
            List<StudentDTO> userTasks = new List<StudentDTO>();

            userTasks = this.dbConexion.Query<StudentDTO>(procedure, commandType: CommandType.StoredProcedure).ToList();
            return userTasks;
        }

        public int GetCountryIdByName(string nameCountry)
        {
            string procedure = "Get_Single_Country_Id";
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@Name", nameCountry);
            int? countryId = this.dbConexion.QuerySingleOrDefault<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
            return countryId==default?0:(int)countryId;
        }

        public StudentEntity GetSingleStudentById(int idStudent)
        {
            string procedure = "Get_Single_Student_By_Id";
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@id", idStudent);

            StudentEntity singleStudent = this.dbConexion.QueryFirstOrDefault<StudentEntity>(procedure, parameters, commandType: CommandType.StoredProcedure);

            return singleStudent;
        }

        public StudentDTO UpdateStudentById(StudentEntity updatedStudent)
        {
            string procedure = "Update_Single_Student_By_Id";
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@id", updatedStudent.id);
            parameters.Add("@Name", updatedStudent.Name);
            parameters.Add("@LastName", updatedStudent.LastName);
            parameters.Add("@CountryId", updatedStudent.CountryId);
            parameters.Add("@CourseId", updatedStudent.CourseId);
            parameters.Add("@CourseAssigned", updatedStudent.CourseAssigned);
            parameters.Add("@Email", updatedStudent.Email);
            parameters.Add("@BirthDay", updatedStudent.BirthDay);
            parameters.Add("@ImageUrl", updatedStudent.ImageUrl);

            StudentDTO userNewTask = this.dbConexion.QuerySingle<StudentDTO>(procedure, parameters, commandType: CommandType.StoredProcedure);
            return userNewTask;
        }
        public StudentDTO GetSingleStudentWithCountryById(int idStudent)
        {
            string procedure = "Get_Single_With_Country_Student_By_Id";
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@id", idStudent);

            StudentDTO singleStudent = this.dbConexion.QueryFirstOrDefault<StudentDTO>(procedure, parameters, commandType: CommandType.StoredProcedure);

            return singleStudent;
        }
    }
}
