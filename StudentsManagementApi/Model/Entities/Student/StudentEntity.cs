﻿namespace Model.Entities.Student
{
    public class StudentEntity
    {
        public int id { get; set; }

        public string Name { get; set; }

        public string LastName { get; set; }

        public int CountryId { get; set; }

        public int? CourseId { get; set; }

        public bool CourseAssigned { get; set; }

        public string Email { get; set; }

        public DateTime BirthDay { get; set; }

        public string ImageUrl { get; set; }
    }
}
