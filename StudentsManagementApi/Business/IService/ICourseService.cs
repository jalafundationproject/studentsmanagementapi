﻿namespace Business
{
    using Model.DTOS.Course;

    public interface ICourseService
    {

        public List<CourseDTO> GetAllCourses();

        public CourseDTO SaveCourse(CreatedCourseDTO newCourse);

        public CourseDTO UpdateCourse(int id, CreatedCourseDTO updateCourse);

        public CourseDTO GetById(int id);

        public void DeleteCourse(int id);

    }
}
