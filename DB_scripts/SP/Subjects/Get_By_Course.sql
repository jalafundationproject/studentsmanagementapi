CREATE PROCEDURE Get_Subject_By_Course
    @IdCourseAssigned INT NULL
AS
SELECT 
    S.Id,
    S.SubjectName,
    S.TrainerName,
    S.ImageUrl,
    S.StartDate,
    S.DaysOfWeek,
    S.StartTime,
    S.FinishTime,
    S.IdCourseAssigned
FROM 
    Subject S
WHERE
    (@IdCourseAssigned is NULL or @IdCourseAssigned = S.IdCourseAssigned)