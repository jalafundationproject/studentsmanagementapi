﻿namespace Repository
{
    using System.Data;
    using System.Data.SqlClient;
    using Dapper;
    using Model.Entities.Course;

    public class CourseRepository:ICourseRepository
    {
        private ConfigurationDB configurationDB;

        private SqlConnection db;

        public CourseRepository()
        {
            this.configurationDB = new ConfigurationDB();
            this.db = new SqlConnection(this.configurationDB.ConnectionString());
        }

        public IEnumerable<CourseEntity> GetAll()
        {
            string procedure = "All_Courses";
            List<CourseEntity> ToDoList = db.Query<CourseEntity>(procedure, commandType: CommandType.StoredProcedure).ToList();
            return ToDoList.OrderByDescending(e => e.Id);
        }

        public CourseEntity SaveCourseInDb(CourseEntity course)
        {
            var procedure = "Add_Course";
            var parameters = new DynamicParameters();
            parameters.Add("@CourseName", course.CourseName, DbType.String, ParameterDirection.Input, 50);
            parameters.Add("@Description", course.Description, DbType.String, ParameterDirection.Input, 300);
            parameters.Add("@StartDate", course.StartDate, DbType.Date, ParameterDirection.Input);
            parameters.Add("@FinishDate", course.FinishDate, DbType.Date, ParameterDirection.Input);
            parameters.Add("@ImagenUrl", course.ImagenUrl, DbType.String, ParameterDirection.Input, 300);
            this.db.Query<CourseEntity>(procedure, parameters, commandType: CommandType.StoredProcedure).ToList();

            return course;
        }

        public CourseEntity GetByName(string name)
        {
            var procedure = "Course_By_Name";
            var parameters = new DynamicParameters();
            parameters.Add("@CourseName", name, DbType.String, ParameterDirection.Input);
            var course = this.db.Query<CourseEntity>(procedure, parameters, commandType: CommandType.StoredProcedure);
            return course.First();
        }

        public CourseEntity GetById(int id)
        {
            var procedure = "Course_By_Id";
            var parameters = new DynamicParameters();
            parameters.Add("@Id", id, DbType.Int32, ParameterDirection.Input);
            var course = this.db.Query<CourseEntity>(procedure, parameters, commandType: CommandType.StoredProcedure);
            return course.First();
        }

        public CourseEntity Update(int id, CourseEntity course)
        {
            var procedure = "Update_Course";
            var parameters = new DynamicParameters();
            parameters.Add("@Id", id, DbType.Int32, ParameterDirection.Input);
            parameters.Add("@CourseName", course.CourseName, DbType.String, ParameterDirection.Input, 50);
            parameters.Add("@Description", course.Description, DbType.String, ParameterDirection.Input, 300);
            parameters.Add("@StartDate", course.StartDate, DbType.Date, ParameterDirection.Input);
            parameters.Add("@FinishDate", course.FinishDate, DbType.Date, ParameterDirection.Input);
            parameters.Add("@ImagenUrl", course.ImagenUrl, DbType.String, ParameterDirection.Input, 300);
            this.db.Execute(procedure, parameters, commandType: CommandType.StoredProcedure);
            return course;
        }

        public void Delete(int id)
        {
            var procedure = "Delete_Course";
            var parameters = new DynamicParameters();
            parameters.Add("@Id", id, DbType.Int32, ParameterDirection.Input);
            this.db.Execute(procedure, parameters, commandType: CommandType.StoredProcedure);
        }
    }
}
