﻿namespace Repository
{
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Configuration.Json;

    internal class ConfigurationDB
    {
        private readonly IConfiguration _config;

        public ConfigurationDB()
        {
            IConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
            configurationBuilder.AddJsonFile("appsettings.json");
            this._config = configurationBuilder.Build();
        }

        public string ConnectionString()
        {
            string connectionString = "";
            connectionString += "Server=" + this._config.GetConnectionString("Server") + ";";
            connectionString += "DataBase=" + this._config.GetConnectionString("DataBase") + ";";
            connectionString += "User=" + this._config.GetConnectionString("User") + ";";
            connectionString += "Password=" + this._config.GetConnectionString("Password") + ";";
            return connectionString;
        }
    }
}
