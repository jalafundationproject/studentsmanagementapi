﻿namespace Business
{
    using Model.DTOS.Student;

    public interface IStudentService
    {
        public StudentDTO CreateSingleStudentService(CreateStudentDTO newStudent);

        public List<StudentDTO> GetAllStudentsService();

        public StudentDTO UpdateSingleStudentService(int idStudent, UpdateStudentDTO updatedStudent);

        public StudentDTO GetSingleStudentServiceById(int idStudent);

    }
}
