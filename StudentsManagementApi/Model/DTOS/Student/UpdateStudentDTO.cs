﻿namespace Model.DTOS.Student
{
    public class UpdateStudentDTO
    {

        public string Name { get; set; }

        public string LastName { get; set; }

        public string Country { get; set; }

        public int? CourseId { get; set; }

        public bool CourseAssigned { get; set; }

        public string Email { get; set; }

        public DateTime BirthDay { get; set; }

        public string ImageUrl { get; set; }
    }
}
