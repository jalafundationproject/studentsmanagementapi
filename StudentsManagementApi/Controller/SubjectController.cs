﻿namespace Controller
{
    using Business;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using Model.Entities;
    using Model.DTOS;

    [ApiController]
    [Route("[controller]")]
    public class SubjectController: ControllerBase
    {
        private readonly ILogger<SubjectController> logger;
        private readonly ISubjectService subjectService;

        public SubjectController(ILogger<SubjectController> logger,ISubjectService subjectService)
        {
            this.logger = logger;
            this.subjectService = subjectService;
        }

        [HttpGet(Name = "GetAllSubject")]
        public IActionResult GetAll()
        {
            try
            {
                List<SubjectDto> res = new List<SubjectDto>();
                this.subjectService.GetAllSubjects().ForEach(subject => res.Add(new SubjectDto(subject)));
                return this.Ok(res);
            }
            catch (Exception)
            {
                return this.StatusCode(500, "Something went worng.");
            }
        }
        [HttpGet("Course",Name = "GetSubjectByCourse")]
        public IActionResult GetByCourse(int idCourse)
        {
            try
            {
                return this.Ok(this.subjectService.GetByCourse(idCourse));
            }
            catch (NotFoundExeption e)
            {
                return this.NotFound(e.Message);
            }
            catch (Exception)
            {
                return this.StatusCode(500, "Something went worng.");
            }
        }

        [HttpPost(Name = "PostSubject")]
        public IActionResult Post([FromBody] CreateSubjectDTO subject)
        {
            try
            {
                return this.Ok(this.subjectService.PostSubjects(subject));
            }
            catch (ArgumentException e)
            {
                return this.Conflict(e.Message);
            }
            catch (BadRequestException e)
            {
                return this.BadRequest(e.Message);
            }
            catch (NotFoundExeption e)
            {
                return this.NotFound(e.Message);
            }
            catch (Exception)
            {
                return this.StatusCode(500, "Something went worng.");
            }
        }
    }
}
