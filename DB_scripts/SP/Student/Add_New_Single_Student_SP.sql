CREATE PROCEDURE Add_New_Single_Student
    @Name VARCHAR(50),
    @LastName VARCHAR(50),
    @CountryId INT,
    @CourseId INT,
    @CourseAssigned BIT,
    @Email VARCHAR(50),
    @BirthDay DATETIME,
    @ImageUrl VARCHAR(150)
AS
BEGIN


INSERT INTO Student(Name, LastName, CountryId, CourseId, CourseAssigned, Email, BirthDay, ImageUrl)
VALUES (@Name, @LastName, @CountryId, @CourseId, @CourseAssigned, @Email, @BirthDay, @ImageUrl);

SELECT S.Id, S.Name, S.LastName, S.CountryId, S.CourseId, S.CourseAssigned, S.Email, S.BirthDay, S.ImageUrl
FROM Student S
WHERE S.Id = SCOPE_IDENTITY()
END

EXEC Add_New_Single_Student 'alejandro', 'zeballos2', 1, null, null, 'corrseo@correo.com', null, 132