CREATE PROCEDURE Select_All_Students
AS
BEGIN
Select S.Id, S.Name, S.LastName, C.Name AS 'Country', S.CourseId, S.CourseAssigned, S.Email, S.BirthDay, S.ImageUrl
FROM Student S INNER JOIN Country C ON S.CountryId = C.Id
END

EXEC Select_All_Students

