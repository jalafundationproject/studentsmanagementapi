﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.DTOS
{
    using Model.Entities;
    using Model.Enum;
    public class SubjectDto
    {
        public int Id { get; set; }

        public string SubjectName { get; set; }

        public string TrainerName { get; set; }

        public DateTime StartDate { get; set; }

        public string ImageUrl { get; set; }

        public List<string> DaysOfWeek { get; set; }

        public TimeSpan StartTime { get; set; }

        public TimeSpan FinishTime { get; set; }

        public int IdCourseAssigned { get; set; }

        public SubjectDto()
        {

        }

        public SubjectDto(Subject subject)
        {
            this.DaysOfWeek= new List<string>();
            this.SubjectName = subject.SubjectName;
            this.TrainerName = subject.TrainerName;
            this.StartDate = subject.StartDate;
            this.ImageUrl = subject.ImageUrl;
            foreach (DaysOfWeek item in System.Enum.GetValues(typeof(DaysOfWeek)))
                if (subject.DaysOfWeek.HasFlag(item))
                    this.DaysOfWeek.Add(item.ToString());
            this.StartTime = subject.StartTime;
            this.FinishTime = subject.FinishTime;
            this.IdCourseAssigned = subject.IdCourseAssigned;
        }
    }
}
