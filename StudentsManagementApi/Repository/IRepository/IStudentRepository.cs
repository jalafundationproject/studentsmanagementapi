﻿namespace Repository
{
    using Model.DTOS.Student;
    using Model.Entities.Student;

    public interface IStudentRepository
    {
        public StudentEntity AddSingleStudent(StudentEntity studentEntityCreated);

        public int GetCountryIdByName(string nameCountry);

        public List<StudentDTO> FindAllStudents();

        public StudentEntity GetSingleStudentById(int idStudent);

        public StudentDTO UpdateStudentById(StudentEntity updatedStudent);
        public StudentDTO GetSingleStudentWithCountryById(int idStudent);
    }
}
