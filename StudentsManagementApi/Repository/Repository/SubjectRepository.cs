﻿namespace Repository
{
    using System.Data;
    using System.Data.SqlClient;
    using Dapper;
    using Model.Entities;

    public class SubjectRepository:ISubjectRepository
    {
        private ConfigurationDB configurationDB;
        private SqlConnection dbConexion;

        public SubjectRepository()
        {
            this.configurationDB = new ConfigurationDB();
            this.dbConexion = new SqlConnection(this.configurationDB.ConnectionString());
        }

        public List<Subject> GetAllSubjects()
        {
            var storageProcedure = "Get_All_Subject";
            return this.dbConexion.Query<Subject>(storageProcedure,commandType: CommandType.StoredProcedure).ToList();
        }

        public List<Subject> GetById(int idCourse)
        {
            var storageProcedure = "Get_Subject_By_Course";
            var parameters = new DynamicParameters();
            parameters.Add("@IdCourseAssigned", idCourse, DbType.Int64, ParameterDirection.Input);
            return this.dbConexion.Query<Subject>(storageProcedure, parameters, commandType: CommandType.StoredProcedure).ToList();
        }

        public Subject PostSubject(Subject subject)
        {
            var storageProcedure = "Post_Subject";
            var parameters = new DynamicParameters();
            parameters.Add("@SubjectName", subject.SubjectName, DbType.String, ParameterDirection.Input, 50);
            parameters.Add("@TrainerName", subject.TrainerName, DbType.String, ParameterDirection.Input, 50);
            parameters.Add("@StartDate", subject.StartDate, DbType.Date, ParameterDirection.Input);
            parameters.Add("@ImageUrl", subject.ImageUrl, DbType.String, ParameterDirection.Input, 100);
            parameters.Add("@DaysOfWeek", subject.DaysOfWeek, DbType.Byte, ParameterDirection.Input);
            parameters.Add("@StartTime", subject.StartTime, DbType.Time, ParameterDirection.Input);
            parameters.Add("@FinishTime", subject.FinishTime, DbType.Time, ParameterDirection.Input);
            parameters.Add("@IdCourseAssigned", subject.IdCourseAssigned, DbType.Int64, ParameterDirection.Input);
            return this.dbConexion.QuerySingle<Subject>(storageProcedure, parameters, commandType: CommandType.StoredProcedure);
        }
    }
}
