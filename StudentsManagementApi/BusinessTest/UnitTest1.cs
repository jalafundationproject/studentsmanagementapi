using Xunit;

namespace BusinessTest
{
    using Business;
    using Repository;
    using Model.Entities;
    using Moq;
    using System.Collections.Generic;

    public class UnitTest1
    {
        private readonly SubjectService subjectService;

        private readonly Mock<ISubjectRepository> mock;

        public UnitTest1()
        {
            this.mock = new Mock<ISubjectRepository>();
            this.subjectService = new SubjectService(this.mock.Object);
        }

        [Fact]
        public void Test1()
        {
            this.mock.Setup((repo) => repo.GetSubject(It.IsAny<int?>(), It.IsAny<int?>())).Returns(new List<Subject>());
            List<Subject> list = this.subjectService.GetAllSubjects(default, 2);
            Assert.NotNull(list);
        }
    }
}