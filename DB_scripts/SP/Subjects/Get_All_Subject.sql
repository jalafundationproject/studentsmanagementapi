CREATE PROCEDURE Get_All_Subject
AS
SELECT 
    S.Id,
    S.SubjectName,
    S.TrainerName,
    S.ImageUrl,
    S.StartDate,
    S.DaysOfWeek,
    S.StartTime,
    S.FinishTime,
    S.IdCourseAssigned
FROM 
    Subject S