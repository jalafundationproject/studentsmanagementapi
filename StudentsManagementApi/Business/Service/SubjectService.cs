﻿namespace Business
{
    using Model.Entities;
    using Model.Validator;
    using Model.DTOS;
    using Repository;
    using FluentValidation.Results;

    public class SubjectService : ISubjectService
    {
        private readonly ISubjectRepository subjectRepository;
        private readonly ICourseService courseService;

        public SubjectService(ISubjectRepository subjectRepository,ICourseService courseService)
        {
            this.subjectRepository = subjectRepository;
            this.courseService = courseService;
        }

        public List<Subject> GetAllSubjects()
        {
            List<Subject> subjects = this.subjectRepository.GetAllSubjects();
            return subjects;
        }

        public List<Subject> GetByCourse(int idCourse)
        {
            if (this.courseService.GetById(idCourse) == default)
            {
                throw new NotFoundExeption($"The Course with id:{idCourse} doesn´t exist");
            }

            List<Subject> subjects = this.subjectRepository.GetById(idCourse);
            return subjects;
        }

        public Subject PostSubjects(CreateSubjectDTO subjectDto)
        {
            List<Subject> subjectsCourse;
            var validationRules = new SubjectValidator();
            Subject subject = new Subject(subjectDto);
            ValidationResult validationResult = validationRules.Validate(subject);
            if (!validationResult.IsValid)
            {
                foreach (var error in validationResult.Errors)
                {
                    throw new ArgumentException(error.ErrorMessage);
                }
            }
            subjectsCourse = this.GetAllSubjects();
            if(subjectsCourse.FirstOrDefault(x => ((x.DaysOfWeek & subject.DaysOfWeek)== x.DaysOfWeek) && this.IsSameTime(x,subject)) != default)
            {
                throw new BadRequestException("A Subject has the same Scheduler");
            }

            if (subjectsCourse.FirstOrDefault(x => x.SubjectName == subject.SubjectName) != default)
            {
                throw new BadRequestException("SubjectName is already in use for this Course");
            }

            return this.subjectRepository.PostSubject(subject);
        }

        private bool IsSameTime(Subject subject1,Subject subject2)
        {
            if (subject1.StartTime >= subject2.FinishTime || subject1.FinishTime <= subject2.StartTime)
            {
                return false;
            }

            return true;
        }


    }
}
