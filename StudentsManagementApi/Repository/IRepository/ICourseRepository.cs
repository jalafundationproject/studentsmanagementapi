﻿using System;
using System.Collections.Generic;
using System.Linq;
namespace Repository
{
    using Model.Entities.Course;

    public interface ICourseRepository
    {
        public IEnumerable<CourseEntity> GetAll();

        public CourseEntity SaveCourseInDb(CourseEntity course);

        public CourseEntity GetByName(string name);

        public CourseEntity GetById(int id);

        public CourseEntity Update(int id, CourseEntity course);

        public void Delete(int id);
    }
}
