CREATE PROCEDURE Get_Single_Country_Id
    @Name VARCHAR(50)
AS
BEGIN
SELECT C.Id
FROM Country C
WHERE C.Name = @Name
END

Exec Get_Single_Country_Id 'Bolivia'
