﻿namespace Model.DTOS
{
    using Model.Enum;

    public class CreateSubjectDTO
    {
        public string SubjectName { get; set; }

        public string TrainerName { get; set; }

        public DateTime StartDate { get; set; }

        public string ImageUrl { get; set; }

        public DaysOfWeek DaysOfWeek { get; set; }

        public string StartTime { get; set; }

        public string FinishTime { get; set; }

        public int IdCourseAssigned { get; set; }
    }
}
