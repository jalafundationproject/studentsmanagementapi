﻿using System;
namespace Business.Exceptions
{
	public class EmptyCourseException : CourseException
	{
		public EmptyCourseException()
		{
			this.ErrorMessage = "The Course Repository is empty!";
		}
	}
}