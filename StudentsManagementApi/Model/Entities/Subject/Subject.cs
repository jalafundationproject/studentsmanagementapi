﻿namespace Model.Entities
{
    using Model.DTOS;
    using Model.Enum;

    public class Subject
    {
        public int Id { get; set; }

        public string SubjectName { get; set; }

        public string TrainerName { get; set; }

        public DateTime StartDate { get; set; }

        public string ImageUrl { get; set; }

        public DaysOfWeek DaysOfWeek { get; set; }

        public TimeSpan StartTime { get; set; }

        public TimeSpan FinishTime { get; set; }

        public int IdCourseAssigned { get; set; }

        public Subject()
        {

        }

        public Subject(CreateSubjectDTO subject)
        {
            this.SubjectName = subject.SubjectName;
            this.TrainerName = subject.TrainerName;
            this.StartDate = subject.StartDate;
            this.ImageUrl = subject.ImageUrl;
            this.DaysOfWeek = subject.DaysOfWeek;
            this.StartTime = TimeSpan.Parse(subject.StartTime);
            this.FinishTime = TimeSpan.Parse(subject.FinishTime);
            this.IdCourseAssigned = subject.IdCourseAssigned;
        }
    }
}
