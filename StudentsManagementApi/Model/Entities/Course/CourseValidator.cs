﻿namespace Model.Entities.Course
{
	using FluentValidation;
    using Model.DTOS.Course;

    public class CourseValidator : AbstractValidator<CourseDTO>
	{
		//List<CourseDTO> _courses = new List<CourseDTO>();
		public CourseValidator()
		{
			//RuleFor(x => x.CourseName).Must(this.ExistCourse).WithMessage("This course already exist");
			RuleFor(x => x.CourseName).MaximumLength(50).WithMessage("The name has more that 50 characters");
			RuleFor(x => x.CourseName).NotEmpty().NotNull().WithMessage("Field is empty");
			RuleFor(x => x.Description).MaximumLength(300).WithMessage("Description has more than 300 characters");
			RuleFor(x => x.Description).NotEmpty().NotNull().WithMessage("Fields is empty");
			RuleFor(x => x.ImagenUrl).MaximumLength(300).WithMessage("Description has more than 300 characters");
			RuleFor(x => x.Description).NotEmpty().NotNull().WithMessage("Fields is empty");
			//_courses = courses;
		}

		//public bool ExistCourse(string name) => this._courses.Any(n => n.CourseName == name) ? false : true;

	}
}