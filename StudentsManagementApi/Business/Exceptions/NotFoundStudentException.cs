﻿namespace Business.Exceptions
{
    public class NotFoundStudentException : Exception
    {
        public string ErrorMessage { get; set; }
    }
}
