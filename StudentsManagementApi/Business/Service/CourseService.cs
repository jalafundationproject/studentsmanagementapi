﻿namespace Business
{
    using Business.Exceptions;
    using FluentValidation;
    using FluentValidation.Results;
    using Model;
    using Model.DTOS.Course;
    using Model.Entities.Course;
    using Repository;


    public class CourseService:ICourseService
    {
        private readonly ICourseRepository courseRepository;

        public CourseService(ICourseRepository courseRepository)
        {
            this.courseRepository = courseRepository;
        }


        public List<CourseDTO> GetAllCourses()
        {
            IEnumerable<CourseEntity> coursesList = this.courseRepository.GetAll();

            if (!coursesList.Any())
            {
                throw new EmptyCourseException();
            }

            List<CourseDTO> courseDTOs = new List<CourseDTO>();

            foreach (var course in coursesList)
            {
                courseDTOs.Add(new CourseDTO()
                {
                    Id = course.Id,
                    CourseName = course.CourseName,
                    Description = course.Description,
                    StartDate = course.StartDate,
                    FinishDate = course.FinishDate,
                    ImagenUrl = course.ImagenUrl,
                    courseState = this.SetCourseState(course.StartDate, course.FinishDate).ToString(),
                });
            }
            return courseDTOs;
        }

        public CourseDTO SaveCourse(CreatedCourseDTO newCourse)
        {
            this.ValidateDate(newCourse.StartDate, newCourse.FinishDate);
            this.ValidateCourseNameExistance(newCourse.CourseName);
            CourseState courseState = this.SetCourseState(newCourse.StartDate, newCourse.FinishDate);
            CourseEntity courseEntity = new CourseEntity()
            {
                CourseName = newCourse.CourseName,
                Description = newCourse.Description,
                StartDate = newCourse.StartDate,
                FinishDate = newCourse.FinishDate,
                ImagenUrl = newCourse.ImagenUrl,

            };

            courseEntity = this.courseRepository.SaveCourseInDb(courseEntity);

            CourseDTO courseCreated = new CourseDTO()
            {
                CourseName = courseEntity.CourseName,
                Description = courseEntity.Description,
                StartDate = courseEntity.StartDate.Date,
                FinishDate = courseEntity.FinishDate.Date,
                ImagenUrl = courseEntity.ImagenUrl,
                courseState = courseState.ToString(),
            };

            this.ValidateCourse(courseCreated);

            return courseCreated;
        }

        public CourseDTO UpdateCourse(int id, CreatedCourseDTO updateCourse)
        {
            this.ValidateDate(updateCourse.StartDate, updateCourse.FinishDate);
            this.ExistanceOfCourseID(id);
            this.ValidateCourseNameExistance(updateCourse.CourseName);
            CourseState courseState = this.SetCourseState(updateCourse.StartDate, updateCourse.FinishDate);
            CourseEntity newCourseEntity = new CourseEntity()
            {
                CourseName = updateCourse.CourseName,
                Description = updateCourse.Description,
                StartDate = updateCourse.StartDate,
                FinishDate = updateCourse.FinishDate,
                ImagenUrl = updateCourse.ImagenUrl,

            };
            CourseDTO courseCreated = new CourseDTO()
            {
                CourseName = newCourseEntity.CourseName,
                Description = newCourseEntity.Description,
                StartDate = newCourseEntity.StartDate.Date,
                FinishDate = newCourseEntity.FinishDate.Date,
                ImagenUrl = newCourseEntity.ImagenUrl,
                courseState = courseState.ToString(),
            };

            this.ValidateCourse(courseCreated);
            this.courseRepository.Update(id, newCourseEntity);

            return courseCreated;
        }

        public CourseDTO GetById(int id)
        {
            CourseEntity courseEntity = this.ExistanceOfCourseID(id);
            CourseDTO courseDTO = new CourseDTO()
            {
                Id = courseEntity.Id,
                CourseName = courseEntity.CourseName,
                Description = courseEntity.Description,
                StartDate = courseEntity.StartDate,
                FinishDate = courseEntity.FinishDate,
                ImagenUrl = courseEntity.ImagenUrl,
                courseState = this.SetCourseState(courseEntity.StartDate, courseEntity.FinishDate).ToString(),
            };

            return courseDTO;
        }

        private void ValidateCourse(CourseDTO course)
        {
            CourseValidator validationRules = new CourseValidator();
            ValidationResult result = validationRules.Validate(course);

            if (!result.IsValid)
            {
                foreach (var error in result.Errors)
                {
                    throw new ArgumentException($"{error.ErrorMessage}");
                }
            }
        }

        public void DeleteCourse(int id)
        {
            this.ExistanceOfCourseID(id);
            this.courseRepository.Delete(id);
        }

        private void ValidateCourseNameExistance(string name)
        {
            var lookingCourseByName = this.GetAllCourses().Where(course => course.CourseName == name);
            if (lookingCourseByName.Any())
            {
                throw new RepeatedCourseException();
            }
        }

        private CourseEntity ExistanceOfCourseID(int id)
        {
            CourseEntity courseEntity = this.courseRepository.GetById(id);
            if (courseEntity == null)
            {
                throw new CourseNotFuntException();
            }

            return courseEntity;
        }

        private void ValidateDate(DateTime startDate, DateTime finisDate)
        {
            if (DateTime.Compare(startDate, finisDate) > 0)
            {
                throw new ArgumentException("The dates cannot be the same");
            }

            if (DateTime.Compare(startDate, finisDate) == 0)
            {
                throw new ArgumentException("Start date cannot be later than finish date");
            }
        }

        private CourseState SetCourseState(DateTime startDate, DateTime finishDate)
        {
            //< 0 − If date1 is earlier than date2.
            //0 − If date1 is the same as date2.
            //> 0 − If date1 is later than date2.
            if (DateTime.Compare(startDate, DateTime.Today) > 0)
            {
                return CourseState.Inactive;
            }
            else if (DateTime.Compare(DateTime.Today, finishDate) > 0)
            {
                return CourseState.Finished;
            }
            else if (DateTime.Compare(startDate, DateTime.Today) <= 0 || DateTime.Compare(DateTime.Today, finishDate) < 0)
            {
                return CourseState.Active;
            }
            else
            {
                return CourseState.Active;
            }
        }
    }
}
