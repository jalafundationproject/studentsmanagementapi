﻿namespace Model.DTOS.Course
{
	public class CreatedCourseDTO
	{
		public int Id { get; }
		public string CourseName { get; set; }
		public string Description { get; set; }
		public DateTime StartDate { get; set; }
		public DateTime FinishDate { get; set; }
		public string ImagenUrl { get; set; }
	}
}