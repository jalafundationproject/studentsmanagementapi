CREATE PROCEDURE Update_Single_Student_By_Id
    @Id INT,
	@Name VARCHAR(50),
    @LastName VARCHAR(50),
    @CountryId INT,
    @CourseId INT,
    @CourseAssigned BIT,
    @Email VARCHAR(50),
    @BirthDay DATETIME,
    @ImageUrl VARCHAR(150)
AS
BEGIN
UPDATE Student
SET Name = @Name, LastName = @LastName, CountryId = @CountryId , CourseId = @CourseId, CourseAssigned = @CourseAssigned, Email = @Email, BirthDay = @BirthDay, ImageUrl = @ImageUrl
WHERE Id = @id;

SELECT Id, Name, LastName, Country= (SELECT Name FROM Country WHERE Id = CountryId), CourseId, CourseAssigned, Email, BirthDay, ImageUrl
FROM Student
WHERE Id = @id
END

EXEC Update_Single_Student_By_Id 6, 'alejandrooo', 'zeballos32', 1, null, null, 'corrseo@correo.com', null, 132