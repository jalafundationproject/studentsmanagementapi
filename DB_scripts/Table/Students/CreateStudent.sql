CREATE Table Student(
    Id INT IDENTITY (1,1),
    Name VARCHAR(50),
    LastName VARCHAR(50),
    CountryId INT,
    CourseId INT,
    CourseAssigned BIT,
    Email VARCHAR(50),
    BirthDay DATETIME,
    ImageUrl VARCHAR(150),
)
