﻿namespace Model.Entities.Student
{
    using FluentValidation;
    using Model.DTOS.Student;

    public class StudentValidator : AbstractValidator<CreateStudentDTO>
    {
        public StudentValidator()
        {
            this.RuleFor(field => field.Name).NotEmpty().WithMessage("Name should not be empty");
            this.RuleFor(field => field.Name).MaximumLength(50).WithMessage("Name should be less than 50 characters");
            this.RuleFor(field => field.LastName).NotEmpty().WithMessage("LastName should not be empty");
            this.RuleFor(field => field.LastName).MaximumLength(75).WithMessage("LastName should be less than 50 characters");
            this.RuleFor(field => field.Email).NotEmpty().WithMessage("Email should not be empty");
            this.RuleFor(field => field.Email).EmailAddress().WithMessage("Email should be in this format correo@correo.com");
            this.RuleFor(field => field.BirthDay).NotEmpty().WithMessage("BirthDay should not be empty");
            this.RuleFor(field => field.BirthDay).LessThan(DateTime.Now).WithMessage("BirthDay should be less than" + DateTime.Now);
            this.RuleFor(field => field.Country).NotEmpty().WithMessage("Country should not be empty");
        }
    }
}
