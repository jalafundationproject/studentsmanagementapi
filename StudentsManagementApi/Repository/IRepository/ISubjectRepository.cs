﻿namespace Repository
{
    using Model.Entities;

    public interface ISubjectRepository
    {
        public List<Subject> GetAllSubjects();

        public List<Subject> GetById(int idCourse);

        public Subject PostSubject(Subject subject);
    }
}
