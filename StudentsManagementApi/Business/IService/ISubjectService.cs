﻿namespace Business
{
    using Model.Entities;
    using Model.DTOS;

    public interface ISubjectService
    {
        public List<Subject> GetAllSubjects();

        public List<Subject> GetByCourse(int idCourse);

        public Subject PostSubjects(CreateSubjectDTO subject);

    }
}
