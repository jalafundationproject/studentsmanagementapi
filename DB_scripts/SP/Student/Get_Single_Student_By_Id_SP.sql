CREATE PROCEDURE Get_Single_Student_By_Id
 @id INT
AS
BEGIN
SELECT Id, Name, LastName, CountryId , CourseId, CourseAssigned, Email, BirthDay, ImageUrl
FROM Student
WHERE Id = @id
END

EXEC Get_Single_Student_By_Id 6