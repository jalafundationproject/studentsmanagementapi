CREATE Table Country(
    Id INT IDENTITY (1,1),
    Name VARCHAR(50),
)

INSERT INTO Country(Name) VALUES
('Bolivia'),
('Argentina'),
('Colombia'),
('Brazil'),
('Ecuador')