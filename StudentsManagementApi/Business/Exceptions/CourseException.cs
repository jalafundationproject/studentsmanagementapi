﻿namespace Business.Exceptions
{
	public abstract class CourseException : Exception
    {
        private string errorMessage;
        private int errorCode;
        public string ErrorMessage
        {
            get
            {
                return errorMessage;
            }
            set
            {
                errorMessage = value;
            }
        }
    }
}