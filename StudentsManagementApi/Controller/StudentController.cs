﻿namespace Controller
{
    using Business;
    using Business.Exceptions;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using Model.DTOS.Student;

    [Route("student")]
    [ApiController]
    public class StudentController : ControllerBase
    {
        private readonly ILogger<StudentController> _logger;
        private readonly IStudentService studentService;

        public StudentController(ILogger<StudentController> logger,IStudentService studentService)
        {
            this._logger = logger;
            this.studentService = studentService;
        }

        [HttpPost]
        public ActionResult PostCreateNewStudent([FromBody] CreateStudentDTO newStudent)
        {
            try
            {
                StudentDTO singleStudent = this.studentService.CreateSingleStudentService(newStudent);
                return this.Ok(singleStudent);
            }
            catch (ArgumentException ex)
            {
                return this.BadRequest(ex.Message);
            }
            catch (Exception)
            {
                return this.StatusCode(500, "Something went worng.");
            }
        }

        [HttpGet]
        public IActionResult GetAllStudents()
        {
            try
            {
                List<StudentDTO> allStudents = this.studentService.GetAllStudentsService();
                return this.Ok(allStudents);
            }
            catch (Exception)
            {
                return this.StatusCode(500, "Something went worng.");
            }
        }

        [HttpPut("{id}")]
        public ActionResult PutUpdateStudent(int id, [FromBody] UpdateStudentDTO updatedStudent)
        {
            try
            {
                StudentDTO newtask = this.studentService.UpdateSingleStudentService(id, updatedStudent);
                return this.Ok(newtask);
            }
            catch (NotFoundStudentException ex)
            {
                return this.NotFound(ex.ErrorMessage);
            }
            catch (ArgumentException ex)
            {
                return this.BadRequest(ex.Message);
            }
            catch (Exception)
            {
                return this.StatusCode(500, "Something went worng.");
            }
        }

        [HttpGet("{id}")]
        public ActionResult GetSingleStudentStudent(int id)
        {
            try
            {
                StudentDTO newtask = this.studentService.GetSingleStudentServiceById(id);
                return this.Ok(newtask);
            }
            catch (NotFoundStudentException ex)
            {
                return this.NotFound(ex.ErrorMessage);
            }
            catch (Exception)
            {
                return this.StatusCode(500, "Something went worng.");
            }
        }
    }
}
