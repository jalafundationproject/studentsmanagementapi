Create Table Course(
    Id INT IDENTITY (1,1),
    CourseName VARCHAR(50),
    Description VARCHAR(300),
    StartDate DATETIME,
    FinishDate DATETIME,
    ImagenUrl VARCHAR(100)
)
