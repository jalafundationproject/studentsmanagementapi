Create Table Subject(
    Id INT IDENTITY (1,1),
    SubjectName VARCHAR(50),
    TrainerName VARCHAR(50),
    StartDate DATE,
    ImageUrl VARCHAR(100),
    DaysOfWeek TINYINT,
    StartTime TIME,
    FinishTime TIME,
    IdCourseAssigned INT
)