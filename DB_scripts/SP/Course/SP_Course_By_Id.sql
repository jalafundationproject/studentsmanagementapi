CREATE PROCEDURE Course_By_Id
    @Id INT
AS
BEGIN
SELECT Id, CourseName , Description,  StartDate, FinishDate, ImagenUrl
FROM Course
WHERE Id = @Id;
END
