﻿namespace Model.DTOS.Student
{
    public class CreateStudentDTO
    {
        public string Name { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public DateTime BirthDay { get; set; }

        public string Country { get; set; }

        public string ImageUrl { get; set; }
    }
}