﻿using System;
namespace Business.Exceptions
{
	public class RepeatedCourseException : CourseException
	{
		public RepeatedCourseException()
		{

			this.ErrorMessage = "The Course already exist";
		}
	}
}

