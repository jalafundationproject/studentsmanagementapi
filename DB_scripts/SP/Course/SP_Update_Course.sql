CREATE PROCEDURE Update_Course
    @Id INT,
    @CourseName VARCHAR(50),
    @Description VARCHAR(300),
    @StartDate DATETIME,
    @FinishDate DATETIME,
    @ImagenUrl VARCHAR(300)

AS
BEGIN
UPDATE Course
SET  
    CourseName  = ISNULL(@CourseName , CourseName),
    Description = ISNULL(@Description, Description), 
    StartDate = ISNULL(@StartDate, StartDate),
    FinishDate = ISNULL(@FinishDate, FinishDate),  
    ImagenUrl =  ISNULL(@ImagenUrl, ImagenUrl)
WHERE Id=@Id
END