﻿namespace Model
{
	public enum CourseState
	{
		Active,
		Inactive,
		Finished,
	}
}