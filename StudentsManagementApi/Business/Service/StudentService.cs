﻿namespace Business
{
    using System.Collections.Generic;
    using Business.Exceptions;
    using FluentValidation.Results;
    using Model.DTOS.Student;
    using Model.Entities.Student;
    using Repository;

    public class StudentService : IStudentService
    {
        private readonly IStudentRepository studentRepository;

        public StudentService(IStudentRepository studentRepository)
        {
            this.studentRepository = studentRepository;
        }

        public StudentDTO CreateSingleStudentService(CreateStudentDTO newStudent)
        {
            this.ValidateStudent(newStudent);
            int countryId = this.studentRepository.GetCountryIdByName(newStudent.Country);
            if (countryId == 0)
            {
                throw new ArgumentException("Country should be in Latam");
            }

            StudentEntity studentEntityCreated = new StudentEntity()
            {   
                Name = newStudent.Name,
                LastName = newStudent.LastName,
                BirthDay = newStudent.BirthDay,
                CourseAssigned = false,
                CourseId = null,
                Email = newStudent.Email,
                CountryId = countryId,
                ImageUrl = newStudent.ImageUrl,
            };
            studentEntityCreated = this.studentRepository.AddSingleStudent(studentEntityCreated);
            StudentDTO studentCreated = new StudentDTO()
            {
                id = studentEntityCreated.id,
                Name = studentEntityCreated.Name,
                LastName = studentEntityCreated.LastName,
                BirthDay = studentEntityCreated.BirthDay,
                CourseAssigned = studentEntityCreated.CourseAssigned,
                CourseId = studentEntityCreated.CourseId,
                Email = studentEntityCreated.Email,
                ImageUrl = studentEntityCreated.ImageUrl,
                Country = newStudent.Country,
            };

            return studentCreated;
        }

        public List<StudentDTO> GetAllStudentsService()
        {
            List<StudentDTO> students = this.studentRepository.FindAllStudents();
            return students;
        }

        public StudentDTO UpdateSingleStudentService(int idStudent, UpdateStudentDTO updatedStudent)
        {
            this.ValidateUpdateStudent(updatedStudent);
            if (this.studentRepository.GetSingleStudentById(idStudent) == null)
            {
                throw new NotFoundStudentException() { ErrorMessage = "Student not found" };
            }

            int countryId = this.studentRepository.GetCountryIdByName(updatedStudent.Country);
            if (countryId == 0)
            {
                throw new ArgumentException("Country should be in Latam");
            }

            StudentEntity studentEntityUpdated = new StudentEntity()
            {
                id = idStudent,
                Name = updatedStudent.Name,
                LastName = updatedStudent.LastName,
                BirthDay = updatedStudent.BirthDay,
                CourseAssigned = updatedStudent.CourseAssigned,
                CourseId = updatedStudent.CourseId,
                Email = updatedStudent.Email,
                CountryId = countryId,
                ImageUrl = updatedStudent.ImageUrl,
            };

            StudentDTO newStudent = this.studentRepository.UpdateStudentById(studentEntityUpdated);
            return newStudent;
        }
        
        
        
        public StudentDTO GetSingleStudentServiceById(int idStudent)
        {
            StudentDTO singleStudent = this.studentRepository.GetSingleStudentWithCountryById(idStudent);
            if (singleStudent == null)
            {
                throw new NotFoundStudentException() { ErrorMessage = "Student not found" };
            }

            return singleStudent;
        }

        private void ValidateStudent(CreateStudentDTO newStudent)
        {
            var validator = new StudentValidator();
            ValidationResult result = validator.Validate(newStudent);
            if (!result.IsValid)
            {
                foreach (var error in result.Errors)
                {
                    throw new ArgumentException(error.ErrorMessage);
                }
            }
        }

        private void ValidateUpdateStudent(UpdateStudentDTO updatedStudent)
        {
            var validator = new UpdateStudentValidator();
            ValidationResult result = validator.Validate(updatedStudent);
            if (!result.IsValid)
            {
                foreach (var error in result.Errors)
                {
                    throw new ArgumentException(error.ErrorMessage);
                }
            }
        }
    }
}
