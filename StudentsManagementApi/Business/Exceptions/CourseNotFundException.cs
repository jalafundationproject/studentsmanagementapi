﻿using System;
namespace Business.Exceptions
{
	public class CourseNotFuntException : CourseException
	{
		public CourseNotFuntException()
		{
			this.ErrorMessage = "Course not fund";
		}
	}
}