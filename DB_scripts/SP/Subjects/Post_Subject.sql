CREATE PROCEDURE Post_Subject
    @SubjectName VARCHAR(50),
    @TrainerName VARCHAR(50),
    @StartDate DATE,
    @ImageUrl VARCHAR(100) NULL,
    @DaysOfWeek TINYINT,
    @StartTime TIME,
    @FinishTime TIME,
    @IdCourseAssigned INT
AS
BEGIN
DECLARE @SubjectId INT 

INSERT INTO Subject (
    SubjectName,
    TrainerName,
    StartDate,
    ImageUrl,
    DaysOfWeek,
    StartTime,
    FinishTime,
    IdCourseAssigned
) VALUES(
    @SubjectName,
    @TrainerName,
    @StartDate,
    @ImageUrl,
    @DaysOfWeek,
    @StartTime,
    @FinishTime,
    @IdCourseAssigned
)

SET @SubjectId = SCOPE_IDENTITY()
SELECT 
    S.Id,
    S.SubjectName,
    S.TrainerName,
    S.ImageUrl,
    S.StartDate,
    S.DaysOfWeek,
    S.StartTime,
    S.FinishTime,
    S.IdCourseAssigned
FROM 
    Subject S
WHERE 
    S.Id = @SubjectId
END