﻿namespace Model.DTOS.Course
{
	public class CourseDTO
	{
		public int Id { get; set; }
		public string CourseName { get; set; }
		public string Description { get; set; }
		public DateTime StartDate { get; set; }
		public DateTime FinishDate { get; set; }
		public string ImagenUrl { get; set; }
		public string courseState { get; set; }
	}
}