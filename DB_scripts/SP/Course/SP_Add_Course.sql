CREATE PROCEDURE Add_Course(
    @CourseName VARCHAR(50),
    @Description VARCHAR(300),
    @StartDate DATETIME,
    @FinishDate DATETIME,
    @ImagenUrl VARCHAR(300)
)
AS
BEGIN
INSERT Course(
    CourseName,
    Description,
    StartDate,
    FinishDate,
    ImagenUrl

)
VALUES(
    @CourseName,
    @Description,
    @StartDate,
    @FinishDate,
    @ImagenUrl
)
END
