﻿namespace Model.Validator
{
    using FluentValidation;
    using Model.Entities;

    public class SubjectValidator: AbstractValidator<Subject>
    {
        public SubjectValidator()
        {
            this.RuleFor(x => x.SubjectName).NotNull().WithMessage("SubjectName can´t be null");
            this.RuleFor(x => x.SubjectName).MaximumLength(50).WithMessage("Maximum length is 50 character");
            this.RuleFor(x => x.TrainerName).NotNull().WithMessage("TrainerName can´t be null");
            this.RuleFor(x => x.TrainerName).MaximumLength(50).WithMessage("Maximum length is 50 character");
            this.RuleFor(x => x.StartDate).NotNull().WithMessage("StartDate can´t be null");
            this.RuleFor(x => x.ImageUrl).MaximumLength(100).WithMessage("Maximum length is 100 character");
            this.RuleFor(x => x.DaysOfWeek).NotNull().WithMessage("DaysOfWeek can´t be null");
            this.RuleFor(x => x.StartTime).NotNull().WithMessage("StartTime can´t be null");
            this.RuleFor(x => x.FinishTime).NotNull().WithMessage("FinishTime can´t be null");
            this.RuleFor(x => x.IdCourseAssigned).NotEmpty().WithMessage("CourseAssigned can´t be null");
            this.RuleFor(x => x.FinishTime).GreaterThan(y => y.StartTime);
        }
    }
}
