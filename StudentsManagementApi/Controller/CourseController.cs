﻿namespace Controller
{
    using Business;
    using Business.Exceptions;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using Model.DTOS.Course;

    [ApiController]
    [Route("course")]
    public class CourseController : ControllerBase
    {
        private readonly ILogger<CourseController> _logger;
        private readonly ICourseService courseService;

        public CourseController(ILogger<CourseController> logger,ICourseService courseService)
        {
            this._logger = logger;
            this.courseService = courseService;

        }

        [HttpGet]
        [ProducesResponseType(typeof(CourseDTO), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public IActionResult GetCourses()
        {
            try
            {
                List<CourseDTO> coursesGotten = this.courseService.GetAllCourses();
                return this.Ok(coursesGotten);
            }
            catch (EmptyCourseException e)
            {
                return this.BadRequest(e.ErrorMessage);
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Error creating new task todo record");
            }

        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult SaveCourse([FromBody] CreatedCourseDTO newCourse)
        {
            try
            {
                CourseDTO courseCreated = this.courseService.SaveCourse(newCourse);
                return this.Created("Course created", courseCreated);
            }
            catch (ArgumentException ex)
            {
                return this.BadRequest(ex.Message);
            }
            catch (RepeatedCourseException ex)
            {
                return this.Conflict(ex.ErrorMessage);
            }
            catch (Exception e)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Error creating new task todo record"+ e.Message);
            }

        }

        [HttpPut("{id}")]
        public IActionResult UpdateCourse(int id, [FromBody]CreatedCourseDTO updateCourse)
        {
            try
            {
                return this.Ok(courseService.UpdateCourse(id, updateCourse));
            }
            catch (ArgumentException e)
            {
                return this.Conflict("I could not find course to update");
            }
            catch (CourseNotFuntException ex)
            {
                return this.NotFound(ex.ErrorMessage);
            }
            catch (Exception)
            {
                return this.BadRequest("Sonme thing whent wrong");
            }
        }


        [HttpDelete("{id}")]
        public IActionResult DeleteCourse(int id)
        {
            try
            {
                this.courseService.DeleteCourse(id);
                return this.Ok();
            }
            catch (CourseNotFuntException ex)
            {
                return this.NotFound(ex.ErrorMessage);
            }
            catch (Exception ex)
            {
                return this.BadRequest(ex.Message);
            }
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(CourseDTO), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public IActionResult GetCourseById(int id)
        {
            try
            {
                return this.Ok(this.courseService.GetById(id));
            }
            catch (CourseNotFuntException ex)
            {
                return this.NotFound(ex.ErrorMessage);
            }
            catch (Exception)
            {
                return this.StatusCode(500, "Somethin went wrong");
            }
        }

    }
}