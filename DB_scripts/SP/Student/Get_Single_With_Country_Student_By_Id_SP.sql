CREATE PROCEDURE Get_Single_With_Country_Student_By_Id
 @id INT
AS
BEGIN
SELECT Id, Name, LastName, Country = (SELECT Name FROM Country WHERE id = CountryId) , CourseId, CourseAssigned, Email, BirthDay, ImageUrl
FROM Student
WHERE Id = @id
END

EXEC Get_Single_With_Country_Student_By_Id 1